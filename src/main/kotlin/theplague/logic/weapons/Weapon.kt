package theplague.logic.weapons

import theplague.logic.Item

abstract class Weapon : Item() {
    abstract override var timesLeft: Int
    abstract override val icon: String
    abstract override fun use()

}
