package theplague.logic.weapons

class Sword(): Weapon() {
    override var timesLeft = 1
    override val icon = "\uD83D\uDDE1"

    override fun use() {
        timesLeft--
    }

}