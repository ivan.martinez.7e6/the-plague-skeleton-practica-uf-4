package theplague.logic.weapons

class Escombra : Weapon() {
    override var timesLeft = 1
    override val icon = "\uD83E\uDDF9"

    override fun use() {
        println(timesLeft)
        timesLeft--
        println(timesLeft)
    }

}