package theplague.logic

import theplague.interfaces.Iconizable

abstract class Item : Iconizable {
    abstract val timesLeft: Int
    abstract override val icon: String
    abstract fun use()
}
