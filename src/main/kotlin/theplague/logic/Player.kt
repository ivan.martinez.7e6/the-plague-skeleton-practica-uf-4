package theplague.logic

import androidx.compose.runtime.currentRecomposeScope
import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.vehicle.OnFoot
import theplague.logic.vehicle.Vehicle
import theplague.logic.weapons.Hand
import theplague.logic.weapons.Weapon

class Player(var position: Position) : IPlayer, Iconizable {

    override var turns = 0
    override var livesLeft: Int = 15

    override var currentWeapon: Weapon = Hand()
    override val weaponTimesLeft: Int get() = currentWeapon.timesLeft
    override var currentVehicle: Vehicle = OnFoot()

    override val icon: String = "\uD83D\uDEB6"

    fun canMove(from: Position, to: Position): Boolean {
        return currentVehicle.canMove(from, to)
    }

    fun move(to: Position) {
        var oldPosition = position
        position = to
        if (currentVehicle.timesLeft != 0) {
            currentVehicle.use()
        } else {
            currentVehicle = OnFoot()
        }

    }

    fun takeDamage(){
        livesLeft --
    }

    // ? name method use
    fun exterminate(): Weapon {
        println("Fase 1: Player /exterminate() : currentWeapon.timesLeft = ${currentWeapon.timesLeft}")
        if (currentWeapon.timesLeft != 0) {
            //currentWeapon.use()
            println("Fase 2: currentweapon.use() : currentWeapon.timesLeft = ${currentWeapon.timesLeft}")
            //currentWeapon = Hand()
        } else if(currentWeapon.timesLeft <= 0)  currentWeapon = Hand()

        //currentWeapon.use()
        return currentWeapon
    }





}
