package theplague.logic.colonies

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.weapons.Weapon


sealed class Colony : Iconizable{
    abstract var size: Int

    abstract fun willReproduce(): Boolean
    abstract fun reproduce()
    abstract fun needsToExpand() : Boolean
    abstract fun attacked(weapon: Weapon)
    abstract fun colonizedBy(plague: Colony): Colony
    abstract fun expand(position: Position, maxPosition: Position): List<Colonization>

}