package theplague.logic.colonies

import theplague.interfaces.Position
import theplague.logic.weapons.Sword
import theplague.logic.weapons.Weapon

class Dragon(override var size: Int) : Colony() {


    override val icon = "\uD83D\uDC09"

    // turn
    private var toReproduce = 0

    override fun willReproduce(): Boolean {

        toReproduce++
        // println("willReproduce toReproduce    $toReproduce")
        // no funciona
        // aparece un dragón por code peno no gráficamente

        return toReproduce >= 5 && size < 3

    }

    override fun reproduce() {
        // println("timeToReproduce  $toReproduce")
        toReproduce = 0
        size++
    }

    override fun needsToExpand(): Boolean {
        return size == 3
        // creo que  deberiamos retornar un Boolean
        // return size == 3
        // pero entonces habria que cambiar la función en Colony
    }

    override fun attacked(weapon: Weapon) {
        if (weapon is Sword) size -= 1
        println(size)
    }

    override fun colonizedBy(plague: Colony): Colony {

        return when (plague){
            is Dragon -> {
                if (plague.size > size){
                    plague
                } else {
                    this
                }
            }

            is Ant -> {
                this
            }
            else -> {
                this
            }
        }
        // return this
    }

    override fun expand(position: Position, maxPosition: Position): List<Colonization> {


        // Never happens
        var x = (0 until maxPosition.x).toMutableList().apply {
            remove(position.x)
        }.random()
        var y = (0 until maxPosition.y).toMutableList().apply {
            remove(position.y)
        }.random()

        /*val posicioX =listOf(position.x,position.x+1, position.x-1).random()
        var posicioY: Int

        if (posicioX ==position.x) {
            posicioY = listOf(position.y+1, position.y-1).random()
        } else {
            posicioY = listOf(position.y,position.y+1, position.y-1).random()
        }*/


        // println("draaaaaaag")
        return listOf(Colonization(Dragon(1), Position(x, y)))


    }
}