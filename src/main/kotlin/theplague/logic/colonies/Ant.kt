package theplague.logic.colonies

import theplague.interfaces.Position
import theplague.logic.weapons.*
import java.util.*

class Ant(override var size: Int) : Colony() {

    override val icon = "\uD83D\uDC1C"
    var reproductionTax: Double = 0.0
    override fun willReproduce(): Boolean {
        return size < 3
    }

    override fun reproduce() {
        val randomGenerator = Random().nextDouble()
        if (randomGenerator <= 0.3 && size < 3) {
            size++
        }
    }

    override fun needsToExpand(): Boolean {
        return size == 3
    }

    override fun attacked(weapon: Weapon) {
        when (weapon) {
            is Sword -> size -= 1
            is Escombra -> size -= 3
            is Hand -> size -= 2
        }
        println(size)
    }

    override fun colonizedBy(plague: Colony): Colony {
        //Mira que el territorio no esté colonizado ya por otra colonia
        return if (plague is Ant) {
            if (plague.size < 3) {
                Ant(plague.size + this.size)
            } else {
                plague
            }
        } else if (plague is Dragon) {
            Dragon(plague.size)
        } else {
            this
        }

    }

    override fun expand(position: Position, maxPosition: Position): List<Colonization> {

        val posicions = mutableListOf(
            Position(position.x + 1, position.y),
            Position(position.x - 1, position.y),
            Position(position.x, position.y + 1),
            Position(position.x, position.y - 1)
        )

        val safePosition = posicions.filter {
            it.x < maxPosition.x && it.x > 0 &&
                    it.y < maxPosition.y && it.y > 0
        }

        return listOf(
            Colonization(
                Ant(1), if (safePosition.size != 0) {
                    safePosition.random()
                } else {
                    posicions.random()
                }
            )
        )


    }
}