package theplague.logic.colonies

import theplague.interfaces.Position

class Colonization(
    val colony: Colony,
    val position: Position
)

