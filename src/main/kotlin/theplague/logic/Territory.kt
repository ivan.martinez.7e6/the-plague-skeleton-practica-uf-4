package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.logic.colonies.Ant
import theplague.logic.colonies.Colony
import theplague.logic.vehicle.OnFoot
import theplague.logic.weapons.Escombra
import theplague.logic.weapons.Hand
import theplague.logic.weapons.Sword
import theplague.logic.weapons.Weapon

class Territory(var width: Int, var height: Int): ITerritory {

    var hasPlayer: Boolean = false


    var colony: Colony? = null


    var item: Item? = null



    fun exterminate (weapon: Weapon){

        colony!!.attacked(weapon)

    }


    override fun iconList(): List<Iconizable> {


        var icon = ""
        if ( hasPlayer){
           //  str += ("$hasPlayer")
        }

        if (colony != null){
            icon +=(colony!!.icon)
        }
        if (item != null){
            icon +=(item!!.icon)
        }


        val listaIconos = mutableListOf<Iconizable>()

        if (hasPlayer){
            listaIconos.add(OnFoot())
        }
        if (item != null){
            listaIconos.add(item!!)
        }
        if (colony != null ){
            repeat(colony!!.size){
                listaIconos.add(colony!!)
            }
        }
        return listaIconos

    }
}
