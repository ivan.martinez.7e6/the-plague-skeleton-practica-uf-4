package theplague.logic.vehicle

import theplague.interfaces.Position


class OnFoot(): Vehicle() {
    override var timesLeft:Int = 2147483647
    override val icon = "\uD83D\uDEB6"
    override fun use() {}

    override fun canMove(from: Position, to: Position): Boolean {
        return to.x in listOf(from.x,from.x+1, from.x-1)
                && to.y in listOf(from.y,from.y+1, from.y-1)

    }
}