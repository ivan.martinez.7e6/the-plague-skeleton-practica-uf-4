package theplague.logic.vehicle

import theplague.interfaces.Position
import theplague.logic.Item


abstract class Vehicle(
) : Item() {
    abstract override val timesLeft: Int
    abstract override val icon: String


    abstract override fun use()
    abstract fun canMove(from: Position, to: Position): Boolean

}
