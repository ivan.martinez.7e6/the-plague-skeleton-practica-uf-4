package theplague.logic.vehicle

import theplague.interfaces.Position


class Bicycle(): Vehicle() {
    override var timesLeft = 5
    override val icon = "\uD83D\uDEB2"
    override fun use() {
        timesLeft--
    }

    override fun canMove(from: Position, to: Position): Boolean {
        return ((to.x in (from.x-4..from.x + 4).toList()
                && (to.y in (from.y-4..from.y + 4).toList())))
    }
}