package theplague.logic.vehicle

import theplague.interfaces.Position


class Helicopter(): Vehicle() {
    override var timesLeft = 5
    override val icon = "\uD83D\uDE81"
    override fun use() {
        timesLeft--
    }

    override fun canMove(from: Position, to: Position): Boolean {
        return true
    }
}