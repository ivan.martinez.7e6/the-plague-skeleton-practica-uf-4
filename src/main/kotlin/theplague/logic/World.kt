package theplague.logic

import theplague.interfaces.*
import theplague.logic.colonies.Ant
import theplague.logic.colonies.Colonization
import theplague.logic.colonies.Colony
import theplague.logic.colonies.Dragon
import theplague.logic.vehicle.Bicycle
import theplague.logic.vehicle.Helicopter
import theplague.logic.vehicle.Vehicle
import theplague.logic.weapons.*
import java.util.Random
import kotlin.math.exp


class World(override val width: Int, override val height: Int) : IWorld {

    override val territories: List<List<Territory>> =
        List(height) { heightInner -> List(width) { widthInner -> Territory(heightInner, widthInner) } }

    private val startingPosition = Position(width / 2, height / 2)
    private var oldPosition: Position = startingPosition
    override val player = Player(startingPosition)
    private val currentTerritory get() = territories[player.position.y][player.position.x]

    init {
        territories[startingPosition.y][startingPosition.x].hasPlayer = true
    }


    override fun nextTurn() {
        territories[oldPosition.y][oldPosition.x].hasPlayer = false
        currentTerritory.hasPlayer = true
        player.turns++
        reproduce()
        expand()
        generateNewColonies()
        generateNewItems()
    }

    override fun gameFinished() : Boolean {
        return (player.livesLeft <= 0)
    }

    override fun canMoveTo(position: Position): Boolean {
        return player.canMove(player.position, position)

    }

    override fun moveTo(position: Position) {
        //TODO() oldPosition = position
        currentTerritory.hasPlayer = false
        player.move(position)
        currentTerritory.hasPlayer = true
    }


    override fun exterminate() {
        //  currentTerritory.colony?.attacked(player.exterminate())
            val colonia = currentTerritory.colony?.size
            if (colonia != null) {

                currentTerritory.exterminate(player.exterminate())
                player.currentWeapon.use()
                if (player.currentWeapon.timesLeft == 0) player.currentWeapon = Hand()

                if (colonia <= 0) {
                    currentTerritory.colony = null
                }
            }
        }


    override fun takeableItem(): Iconizable? {
        return currentTerritory.item
    }

    override fun takeItem() {
        when (val item = currentTerritory.item) {
            is Vehicle -> player.currentVehicle = item
            is Weapon -> player.currentWeapon = item
        }
        currentTerritory.item = null
    }

    private fun randomPosition(isSomething: Boolean = true): Position {
        // TODO("val place = territories[it.y][it.x]
        //                        place.colony == null
        //                                &&
        //                                place.item == null")
        if (isSomething) {
            while (true) {
                val emptyTerritories = territories.mapIndexed { indexY, territoriesL ->
                    val list = List(territoriesL.size) { indexX ->
                        Position(indexX, indexY )
                    }
                    // println("sizee ${list.size}")
                    list.filter { it ->
                        /*println("y ${it.y}")
                        println("x ${it.x}")*/

                        val place = territories[it.y][it.x]
                        place.colony == null
                                &&
                                place.item == null
                    }
                }.flatten()

                return if (emptyTerritories.isNotEmpty())
                    emptyTerritories.random()
                else Position(
                    (0 until width).random(),
                    (0 until height).random()
                )
            }
        } else {
            return Position(
                (0 until width).random(),
                (0 until height).random()
            )
        }
    }

    private fun generateNewColonies() {
        val colony: Colony?

        val randomGenerator = Random().nextDouble()

        colony = if (randomGenerator < 0.1) Dragon(1)
        else if (randomGenerator < 0.4) Ant(1)
        else null

        val posRandom = randomPosition()
        if (territories[posRandom.y][posRandom.x].item == null) {
            territories[posRandom.y][posRandom.x].colony = colony
            // println(territories[posRandom.y][posRandom.x].colony)
        }

    }

    private fun place(colonization: Colonization) {
        try {
            if (territories[colonization.position.y][colonization.position.x].colony == null) {
                territories[colonization.position.y][colonization.position.x].colony = colonization.colony
            } else {
                territories[colonization.position.y][colonization.position.x].colony!!.colonizedBy(colonization.colony)
            }
        } catch (e: Exception) {
            println(e)
            println(colonization.position.x)
            println(colonization.position.y)
        }


    }

    private fun generateNewItems() {
        val item: Item?

        val randomGenerator = Random().nextDouble()

        item = if (randomGenerator < 0.1) Sword()
        else if (randomGenerator < 0.2) Helicopter()
        else if (randomGenerator < 0.45) Escombra()
        else if (randomGenerator < 0.70) Bicycle()
        else null

        val posRandom = randomPosition(true)
        if (territories[posRandom.y][posRandom.x].colony == null) {
            territories[posRandom.y][posRandom.x].item = item
            // println(territories[posRandom.y][posRandom.x].item)
        }


    }
// width: Int, override val height
    private fun reproduce() {
        for (height in territories.indices) {
            for (width in territories[height].indices) {
                if (territories[height][width].colony != null) {
                    if (territories[height][width].colony!!.willReproduce()) {
                        territories[height][width].colony!!.reproduce()
                    }
                }

            }

        }
    }

    private fun expand() {

        for (height in territories.indices) {
            for (width in territories[height].indices) {
                territories[height][width].colony?.let { colony ->
                    if (colony.needsToExpand()) {
                        val colonizations = colony.expand(Position(width, height), Position(this.width, this.height))
                        for (colonization in colonizations) {
                            place(colonization)
                            player.takeDamage()
                        }
                    }
                }
            }
        }

    }


}

